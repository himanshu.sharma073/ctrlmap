import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CommentComponent } from './components/comments/comments.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TimeDateFormatterPipe } from './pipes/time-date-formatter.pipe';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

const appRoutes: Routes = [
  { path: 'tasks', component: CommentFormComponent },
   {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },

  { path: 'dashboard', component: DashboardComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    CommentFormComponent,
    PageNotFoundComponent,
    DashboardComponent,
    CommentComponent,
    HeaderComponent,
    FooterComponent,
    TimeDateFormatterPipe,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    RichTextEditorAllModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],
  exports: [RouterModule],

  bootstrap: [AppComponent],
})
export class AppModule {}
