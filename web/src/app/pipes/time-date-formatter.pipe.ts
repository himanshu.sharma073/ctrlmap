import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeDateFormatter'
})
export class TimeDateFormatterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value){
      return this.formatCommentDateTime(new Date(),new Date(value.toLocaleString()));
    }
   }

  formatCommentDateTime(currentTime, previousTime) {
    
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;
    
    var elapsed = currentTime - previousTime;
    
    if (elapsed < msPerMinute) {
         return Math.round(elapsed/1000) + ' second(s) ago';   
    }
    
    else if (elapsed < msPerHour) {
         return Math.round(elapsed/msPerMinute) + ' minute(s) ago';   
    }
    
    else if (elapsed < msPerDay ) {
         return Math.round(elapsed/msPerHour ) + ' hour(s) ago';   
    }

    else if (elapsed < msPerMonth) {
         return Math.round(elapsed/msPerDay) + ' day(s) ago';   
    }
    
    else if (elapsed < msPerYear) {
         return Math.round(elapsed/msPerMonth) + ' month(s) ago';   
    }
    
    else {
         return Math.round(elapsed/msPerYear ) + ' year(s) ago';   
    }
  }


}
