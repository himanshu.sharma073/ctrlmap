import { Component, OnInit, OnDestroy, Output,EventEmitter } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { Comment } from '../../models/comment.model';
import { Subscription } from 'rxjs';
import { Input } from '@angular/core';


@Component({
  selector: 'app-comment',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
  providers: [CommentService],
})
export class CommentComponent implements OnInit, OnDestroy {

  @Output() deleteMessage = new EventEmitter<string>();
  
  public comments: Comment[] = [];
  public showMenu:Boolean = false;
  public showRow=-1;
  public popoverTitle: string = 'Delete Comment?';
  public popoverMessage: string = 'Deleting comment is forever. There is no undo.';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  @Input() taskid;
  @Input() userid;
  @Output() ceditComment = new EventEmitter<Comment>();
  private commentSubscription: Subscription;
  
  constructor(private commentService: CommentService) {
    this.commentSubscription = commentService
      .getCommentItems()
      .subscribe((comment: Comment) => {
        if(comment.status.toLowerCase() =='deleted'){
          let deletedComment = this.comments.find(c=>c.commentId==comment.commentId);
          this.comments.splice(this.comments.indexOf(deletedComment),1);
        }else if(comment.status.toLowerCase() =='updated'){
          let updateComment = this.comments.find(c=>c.commentId==comment.commentId);
          updateComment.commentBody = comment.commentBody;
        }else if(comment.status.toLowerCase() =='created'){
          this.comments.push(comment);
        }
        
      });
  }

  ngOnInit() {
    this.commentService.findCommentsByTaskid(this.taskid).subscribe(data => {
      this.comments = data;
      console.log(data);
    }); 

  }
  
  deleteComment(comment:Comment){
    this.commentService.deleteCommentsByCommentId(comment).subscribe(res=>{
      console.log('comment deleted successfully::',res);
      this.deleteMessage.emit("Comment is deleted successfully....");
      this.showMenu=false;
    },error=>{
      console.log('errror in deleting comment::',error);
      this.deleteMessage.emit("Error while deleted the message....");
      this.showMenu=false;
    })

  }
  ngOnDestroy() {
    this.commentSubscription.unsubscribe();
  }

  public showEditComment(comment:Comment){
    this.ceditComment.emit(JSON.parse(JSON.stringify(comment)));
  }

  
   

}