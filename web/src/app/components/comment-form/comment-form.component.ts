import { Component, OnInit, Injectable } from '@angular/core';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService, QuickToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { CommentService } from '../../services/comment.service';
import { Comment } from '../../models/comment.model';
@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService, QuickToolbarService],
})
export class CommentFormComponent implements OnInit {
  private isSending: boolean;

  public content: string;
  public errorMsg: string;
  public infoMsg: string;
  public title: string;
  public comment: Comment = new Comment();

  public comments: Comment[];
  isEditing: Boolean = false;
  buttonEnable:Boolean=true;
  constructor(private commentService: CommentService) {

  }

  public tools: object = {
    items: ['Undo', 'Redo', '|',
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
      'SubScript', 'SuperScript', '|',
      'LowerCase', 'UpperCase', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable',
      'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen']
  };
  public value: string = "";
  public quickTools: object = {
    image: [
      'Replace', 'Align', 'Caption', 'Remove', 'InsertLink', '-', 'Display', 'AltText', 'Dimension']
  };

  public updateTextAreaComment(){
        //var ss="<p><br><span class=\"e-editor-select-start\"></span></p>";
        //console.log(ss.length);
        console.log("_@)@))@)@@)"+this.comment.commentBody.length);
        if(this.comment.commentBody && this.comment.commentBody.length>54){
          this.buttonEnable=false;
          this.infoMsg="";
        }else{
          this.buttonEnable=true;
        }
  }
  postComment() {
    if (this.comment.commentBody && this.comment.commentBody.length > 0) {
      var encodedString: String = '';
      this.errorMsg = '';
      this.isSending = true;
      this.infoMsg = 'Processing your request.. Wait a minute';
      // encodedString = btoa(this.content);

      this.commentService.saveComment(this.comment).subscribe(data => {
        this.infoMsg = data.message;
        this.comment.commentBody = "";
        // this.comment.
      }, error => {
        this.infoMsg = error.message;
      }).add(() => {
        this.isSending = false;
        this.clearAlertMessage();
      
      });
    } else {
      this.infoMsg = "Comment can't be blank";
    }
  }

  public deleteComment() {
    this.commentService.deleteCommentsByCommentId(this.comment).subscribe(data => {
      this.infoMsg = data.message;
    }, error => {
      this.infoMsg = error.message;
    }).add(() => {
      this.isSending = false;
      this.clearAlertMessage();
    
    });
  }

  public deleteMessage(event){
    this.infoMsg = event;
    this.clearAlertMessage();
  }

  public updateComment() {
    this.commentService.updateCommentsByCommentId(this.comment).subscribe(data => {
      this.infoMsg = data.message;
      this.isEditing = false;
    }, error => {
      this.infoMsg = error.message;
    }).add(() => {
      this.isSending = false;
      this.clearAlertMessage();
    
    });
  }
  public resetComment() {
    this.comment.commentBody = "";
  }

  ngOnInit() {
    this.comment.taskid = "dummytask";
    this.comment.userid = "himanshu.sharma";

  }

  clearAlertMessage(){
    setTimeout(()=>{
      this.infoMsg="";
    },3000);
  }
}
