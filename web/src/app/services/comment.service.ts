import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Comment } from '../models/comment.model';
import Pusher from 'pusher-js';
import {AppConfig} from '../config/app.config';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  
  private commentSubject: Subject<Comment> = new Subject<Comment>();

  private pusherClient: Pusher;

  constructor(private http: HttpClient) {
    this.pusherClient = new Pusher(AppConfig.PUSHER_APP_KEY, { cluster: AppConfig.PUSHER_APP_CLUSTER });
    const channel = this.pusherClient.subscribe(AppConfig.PUSHER_APP_SUBSCRIBER);

    channel.bind(
      AppConfig.PUSHER_COMMENT_CHANNEL,
      (data: {value:{ commentId: string; commentBody: string;userid: string;taskid: string; updatedTimeStamp: string},event:string }) => {
        this.commentSubject.next(new Comment(data.value.commentId, data.value.commentBody, data.value.userid,data.value.taskid,new Date(data.value.updatedTimeStamp),data.event));
      }
    );
  }

  getCommentItems(): Observable<Comment> {
    return this.commentSubject.asObservable();
  }


  public saveComment(comment:Comment):Observable<any>{
    const endpoint = AppConfig.REST_API_ENDPOINT+"/tasks/"+comment.taskid+"/comments";
    //const httpOptions = {headers:new HttpHeaders({'user-access-token':'T0010101010','Content-Type':'application/json','Accept':'application/json'})};
    return this.http.post(endpoint,comment);
  }


  public findCommentsByTaskid(taskid:String):Observable<Comment[]>{
     const endpoint = AppConfig.REST_API_ENDPOINT+"/tasks/"+taskid+"/comments";
     //const httpOptions = {headers:new HttpHeaders({'user-access-token':'T0010101010','Content-Type':'application/json','Accept':'application/json'})};
      return this.http.get<Comment[]>(endpoint);
  }

  public deleteCommentsByCommentId(comment:Comment):Observable<any>{
    const endpoint = AppConfig.REST_API_ENDPOINT+"/tasks/"+comment.taskid+"/comments/"+comment.commentId;
    //const httpOptions = {headers:new HttpHeaders({'user-access-token':'T0010101010','Content-Type':'application/json','Accept':'application/json'})};
    return this.http.delete(endpoint);
 }

 public updateCommentsByCommentId(comment:Comment):Observable<any>{
  const endpoint = AppConfig.REST_API_ENDPOINT+"/tasks/"+comment.taskid+"/comments";
  //const httpOptions = {headers:new HttpHeaders({'user-access-token':'T0010101010','Content-Type':'application/json','Accept':'application/json'})};
  return this.http.put(endpoint,comment);
}


}
