export class AppConfig {
    public static REST_API_ENDPOINT = "http://localhost:4000/v3";
    
    public static DELETE_USER_ENDPOINT = "http://localhost:4000/" + 'deleteUser';

    public static PUSHER_APP_ID=768819;
    public static PUSHER_APP_KEY='c4f2c2977c698d3a9e2e';
    public static PUSHER_APP_SECRET='c6986c861a76f2c19383';
    public static PUSHER_APP_CLUSTER='ap2';
    public static PUSHER_APP_SECURE=1;
    public static PUSHER_APP_SUBSCRIBER = 'dummytask';
    public static PUSHER_COMMENT_CHANNEL = 'comments-channel';
}
