
//Importing schema..
var CommentsEntity=require('../odm/comments-entity');
var logger = require('logger').createLogger();
//saving the comments into the database
exports.saveComment=function(comment,callback) {
    var commentsEntity=new CommentsEntity();
    commentsEntity.commentBody=comment.commentBody;
    commentsEntity.userid=comment.userid;
    commentsEntity.taskid=comment.taskid;
    commentsEntity.commentId=comment.commentId;
    commentsEntity.save(function(err,data){
        callback(err,data);
    });
}

exports.findCommentsByTaskId=function(taskid,callback) {
    if(taskid) {
        CommentsEntity.find({taskid:taskid},function(err,data){
            callback(err,data);
    }).sort({createdTimeStamp: 'ascending'});
    }else{
        CommentsEntity.find({},function(err,data){
            callback(err,data);
    });
    }
}  


/**
 * _mid is  mongodb id
 */
exports.findCommentsById=function(_mid,callback) {
    CommentsEntity.findById(_mid,function(err,data){
        callback(err,data);
          });
        
}  


exports.findCommentsByCommentId=function(taskid,commentId,callback) {
    CommentsEntity.findOne({taskid:taskid,commentId: commentId}, function(err,data){
        callback(err,data);
    });    
}  

exports.findComments=function(callback) {
    CommentsEntity.find({}, function(err,data){
        callback(err,data);
    });    
} 



exports.deleteCommentsByCommentId=function(taskid,commentId,callback) {
    CommentsEntity.findOneAndDelete({taskid:taskid,commentId: commentId}, function(err){
        callback(err);
    });    
}  

exports.deleteCommentsByMid=function(pmid,callback) {
    CommentsEntity.findAndRemove({mid: pmid}, function(err){
        callback(err);
    });    
}  

exports.updateComments=function(comments,callback){
    logger.info('updateing');
    if(comments.commentId!=undefined){
        CommentsEntity.updateOne({commentId:comments.commentId,taskid:comments.taskid},{
            commentBody:comments.commentBody,
            userid:comments.userid
        },(err , comment)=>{
            callback(err,comment);
        });
    }
    else{
        callback("invalid update operation");
    }
}