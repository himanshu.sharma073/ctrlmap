
//Importing schema..
var UsersEntity=require('../odm/users-entity');
var logger = require('logger').createLogger();

//saving the users into the database
exports.saveUser=function(user,callback) {
    var usersEntity=new UsersEntity();
    usersEntity.userid=user.userid;
    usersEntity.name=user.name;
    usersEntity.email=user.email;
    usersEntity.save(function(err,data){
        callback(err,data);
    });
}

exports.findUserByUserId=function(userid,callback) {
    if(userid) {
        UsersEntity.findOne({userid:userid},function(err,data){
            callback(err,data);
    }).sort({createdTimeStamp: 'desc'});
}

}  

exports.findUsers=function(callback) {
    UsersEntity.find({}, function(err,data){
        callback(err,data);
    });    
} 



exports.deleteUserByUserId=function(userid,callback) {
    UsersEntity.findOneAndDelete({userid: userid}, function(err){
        callback(err);
    });    
}  

exports.updateUser=function(user,callback){
    logger.info('updateing');
    if(user.userid){
        UsersEntity.updateOne({userid:user.userid},{
            name:user.name,
            email:user.email
        },(err)=>{
            callback(err);
        });
    }
    else{
        callback("invalid update operation");
    }
}