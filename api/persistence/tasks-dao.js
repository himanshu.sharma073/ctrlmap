//Importing schema..
var TasksEntity=require('../odm/tasks-entity');
var logger = require('logger').createLogger();

//saving the tasks into the database
exports.saveTask=function(task,callback) {
    var tasksEntity=new TasksEntity();
    tasksEntity.taskid=task.taskid;
    tasksEntity.owner=task.owner;
    tasksEntity.assignee=task.assignee;
    tasksEntity.save(function(err,data){
        callback(err,data);
    });
}

exports.findTaskByTaskId=function(taskid,callback) {
    if(taskid) {
        TasksEntity.findOne({taskid:taskid},function(err,data){
            callback(err,data);
    }).sort({createdTimeStamp: 'desc'});
}  
}

exports.findTasks=function(callback) {
    TasksEntity.find({}, function(err,data){
        callback(err,data);
    });    
} 



exports.deleteTaskByTaskId=function(taskid,callback) {
    TasksEntity.findOneAndDelete({taskid: taskid}, function(err){
        callback(err);
    });    
}  

exports.updateTask=function(task,callback){
    logger.info('updateing');
    if(task.taskid){
        TasksEntity.updateOne({taskd:task.taskid},{
            assignedto:task.assignedto,
            owner:task.owner
        },(err)=>{
            callback(err);
        });
    }
    else{
        callback("invalid update operation");
    }
}