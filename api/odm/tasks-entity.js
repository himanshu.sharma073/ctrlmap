/**
 *  Define schema for tasks
 *  connection to the database
 *  This file is known as module
 */
//creating mongoose to define schema
//This mongoose is ORM for 
var mongoose = require('mongoose');
var TasksSchema  = new mongoose.Schema({
    taskid : { type: String,required: true, unique: true },
    owner : { type: String},
    assignee : { type: String},
    createdTimeStamp : {type: Date},
    updatedTimeStamp: {type: Date},
    },{collection: 'tasks'});

            //on every save, add the date
    TasksSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();
    // change the updated_at field to current date
    this.updatedTimeStamp = currentDate;
    // if created_at doesn't exist, add to that field
    if (!this.createdTimeStamp){
      this.createdTimeStamp = currentDate;
    } 
    next(); //means go ahead and save it into db now
});

//Here we are registering TasksSchema as model in mongoose
var TasksEntity=mongoose.model('tasks', TasksSchema);
//exporting object TasksEntity
module.exports=TasksEntity;

