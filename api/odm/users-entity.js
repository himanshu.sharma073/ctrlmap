/**
 *  Define schema for users
 *  connection to the database
 *  This file is known as module
 */
//creating mongoose to define schema
//This mongoose is ORM for 
var mongoose = require('mongoose');
var UsersSchema  = new mongoose.Schema({
    userid : { type: String,required: true, unique: true },
    name : { type: String},
    email : { type: String},
    createdTimeStamp : {type: Date},
    updatedTimeStamp: {type: Date},
    },{collection: 'users'});

            //on every save, add the date
    UsersSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();
    // change the updated_at field to current date
    this.updatedTimeStamp = currentDate;
    // if created_at doesn't exist, add to that field
    if (!this.createdTimeStamp){
      this.createdTimeStamp = currentDate;
    } 
    next(); //means go ahead and save it into db now
});

//Here we are registering UsersSchema as model in mongoose
var UsersEntity=mongoose.model('users', UsersSchema);
//exporting object UsersEntity
module.exports=UsersEntity;

