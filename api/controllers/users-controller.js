var UsersService = require("../services/users-service");

//to generate unique id
var uniqid = require('uniqid');

var logger = require('logger').createLogger();  

/**
 * This method is used to save users
 * 
 * */
exports.saveUser=function(req,res) {
     //reading form data coming from request body
    var user=req.body;
    UsersService.saveUser(user,function(err,data){
        if(err){
            logger.setLevel('error');
            logger.error(err);
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            logger.info('user saved :' + user.taskid);
            const message={status:"success",message:"User is saved successfully",_id:data._id};
            return res.status(200).send(message);
        }
    });
};

exports.findUserByUserId=function(req,res) {
        var userId=req.params.userid;
        UsersService.findUserByUserId(userId,function(err,ddata){
                if(err){
                    var response={status:"fail",message:err};
                    res.json(response);
                }else{
                    var response={status:"success",data:ddata};
                    res.json(response);
                }
            });
        
};

exports.deleteUserByUid=function(req,res) {
    var userid=req.params.userid;
    UsersService.deleteUserByUid(userid,function(err){
        if(err){
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            const response = {
                message: "user is deleted successfully with user id "+userid,
                status: "success"
            };
            return res.status(200).send(response);
        }
    });
};

exports.updateUser=function(req,res){
    //reading  data coming from request body
    var user=req.body;
    user.userid = req.params.userid;
    UsersService.updateUser(user,function(err){
        if(err){
            var message={status:"fail",message:err};
            res.json(message);
        }else{
            var message={status:"success",message:"Users is updated successfully"};
            res.json(message);
        }
    });
}


exports.findUsers=function(req,res) {
    UsersService.findUsers(function(err,ddata){
            if(err){
                var response={status:"fail",message:err};
                res.json(response);
            }else{
                var response={status:"success",data:ddata};
                res.json(response);
            }
        });
    
};
