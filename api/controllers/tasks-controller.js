var TasksService = require("../services/tasks-service");

//to generate unique id
var uniqid = require('uniqid');

var logger = require('logger').createLogger();  

/**
 * This method is used to save tasks
 * 
 * */
exports.saveTask=function(req,res) {
     //reading form data coming from request body
    var task=req.body;
    //setting unique commentid
    task.taskid=uniqid();
    TasksService.saveTask(task,function(err,data){
        if(err){
            logger.setLevel('error');
            logger.error(err);
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            logger.info('task saved :' + task.taskid);
            const message={status:"success",message:"Task is saved successfully",_id:data._id};
            return res.status(200).send(message);
        }
    });
};

exports.findTaskByTaskId=function(req,res) {
        var taskId=req.params.taskid;
        TasksService.findTaskByTaskId(taskId,function(err,ddata){
                if(err){
                    var response={status:"fail",message:err};
                    res.json(response);
                }else{
                    var response={status:"success",data:ddata};
                    res.json(response);
                }
            });
        
};

exports.deleteTaskByTid=function(req,res) {
    var taskid=req.params.taskid;
    TasksService.deleteTaskByTid(taskid,function(err){
        if(err){
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            const response = {
                message: "task is deleted successfully with task id "+taskid,
                status: "success"
            };
            return res.status(200).send(response);
        }
    });
};

exports.updateTask=function(req,res){
    //reading  data coming from request body
    var task=req.body;
    task.taskid = req.params.taskid;
    TasksService.updateTask(task,function(err){
        if(err){
            var message={status:"fail",message:err};
            res.json(message);
        }else{
            var message={status:"success",message:"Tasks is updated successfully"};
            res.json(message);
        }
    });
}


exports.findTasks=function(req,res) {
    TasksService.findTasks(function(err,ddata){
            if(err){
                var response={status:"fail",message:err};
                res.json(response);
            }else{
                var response={status:"success",data:ddata};
                res.json(response);
            }
        });
    
};
