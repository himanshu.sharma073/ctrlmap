var CommentsService = require("../services/comments-service");

//to generate unique id
var uniqid = require('uniqid');

var logger = require('logger').createLogger();

var Pusher = require('pusher');

var pusher = new Pusher({
    appId: '768819',
    key: 'c4f2c2977c698d3a9e2e',
    secret: 'c6986c861a76f2c19383',
    cluster: 'ap2',
    encrypted: true
  });
  

/**
 * This method is used to save comments
 * 
 * */
exports.saveComment=function(req,res) {
     //reading form data coming from request body
    var comment=req.body;
    comment.taskid = req.params.taskid;
    //setting unique commentid
    comment.commentId=uniqid();
   // comment.taskid=taskid;
    CommentsService.saveComment(comment,function(err,data){
        if(err){
            logger.setLevel('error');
            logger.error(err);
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            logger.info('comment saved for task :' + comment.taskid);
            CommentsService.findCommentsByCommentId(comment.taskid, comment.commentId, function(err,data) {
                pusher.trigger(comment.taskid, 'comments-channel', {event:"created", value:data});
                const message={status:"success",message:"Comment is saved successfully",commentId:data.commentId};
                return res.status(200).send(message);
             });

        }
    });
};

exports.findCommentsByCommentId=function(req,res) {
        var commentId=req.params.cid;
        var taskid=req.params.taskid;
        CommentsService.findCommentsByCommentId(taskid,commentId,function(err,ddata){
                if(err){
                    var response={status:"fail",message:err};
                    res.json(response);
                }else{
                    var response={status:"success",data:ddata};
                    res.json(response);
                }
            });
        
};

//task id is coming as a part of URI
exports.findCommentsByTaskId=function(req,res) {
    var taskid=req.params.taskid;
    CommentsService.findCommentsByTaskId(taskid,function(err,comments){
        if(err){
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            res.send(comments);         
        }
    });
};


//@PathVariable
///comments/3123123
//express.delete("/comments/:cid"
exports.deleteCommentsByCid=function(req,res) {
    var cid=req.params.cid;
    var taskid=req.params.taskid;
    logger.info("cid = "+cid);
    logger.info("taskid = "+taskid);
     if(cid==undefined || taskid==undefined){
        const response={status:"fail",message:"cid cannot be empty"};
        res.status(500).json(response);
    }
    CommentsService.deleteCommentsByCid(taskid,cid,function(err){
        if(err){
            const response={status:"fail",message:err};
            res.status(500).json(response);
        }else{
            pusher.trigger(taskid, 'comments-channel', {event:"deleted", value:{commentId: cid}});
            const response = {
                message: "comment is deleted successfully with comment id "+cid,
                status: "success"
            };
            return res.status(200).send(response);
        }
    });
};

exports.updateComments=function(req,res){
    //reading  data coming from request body
    var comments=req.body;
    comments.taskid = req.params.taskid;
    CommentsService.updateComments(comments,function(err){
        if(err){
            var message={status:"fail",message:err};
            res.json(message);
        }else{
            CommentsService.findCommentsByCommentId(comments.taskid, comments.commentId, function(err,data) {
            pusher.trigger(comments.taskid, 'comments-channel', {event:"updated", value:data});
            var message={status:"success",message:"Comments is updated successfully"};
            res.json(message);
            });
        }
    });
}


exports.findComments=function(req,res) {
    CommentsService.findComments(function(err,ddata){
            if(err){
                var response={status:"fail",message:err};
                res.json(response);
            }else{
                var response={status:"success",data:ddata};
                res.json(response);
            }
        });
    
};
