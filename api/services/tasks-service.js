var TasksDao = require("../persistence/tasks-dao");
//saving the tasks into the database
exports.saveTask=function(task,callback) {
    TasksDao.saveTask(task,function(err,data) {
        callback(err,data);
    });
};   

exports.findTaskByTaskId=function(taskId,callback){
    TasksDao.findTaskByTaskId(taskId,function(err,ddata){
        callback(err,ddata);
    });
};

exports.findTasks=function(callback){
    TasksDao.findTasks(function(err,ddata){
        callback(err,ddata);
    });
};

exports.deleteTaskByTid=function(taskid,callback) {
    TasksDao.deleteTaskByTaskId(taskid,function(err){
        callback(err);
    });      
};

exports.updateTask=function(task,callback){
    TasksDao.updateTask(task,function(err){
        callback(err);
    });      
};   