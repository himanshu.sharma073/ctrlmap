

var CommentEmailService = require('../services/comments-mail-service');
var TasksService = require("../services/tasks-service");
var UsersService = require("../services/users-service");
var CommentsDao = require("../persistence/comments-dao");
var AppConfig = require('../config/app-config')

var logger = require('logger').createLogger();
//saving the comments into the database
exports.saveComment = function (comment, callback) {

    TasksService.findTaskByTaskId(comment.taskid, function (err, data) {
        if (!data || err) {
            err = "Invalid taskid";
            callback(err, data);
        } else {
            let task = data;
            let userid;
            let valid = false;
            if (comment.userid === task.owner) {
                userid = task.assignee;
                valid = true;
            } else if (comment.userid === task.assignee) {
                userid = task.owner;
                valid = true;
            }
            if (!valid) {
                err = "Invalid user or the user is neither the owner nor the assignee of task";
                callback(err, data);
            }
            else {
                    logger.info("Saving comment");
                CommentsDao.saveComment(comment, function (err, data) {
                    if (data) {
                        UsersService.findUserByUserId(userid, function (err, datau) {
                            if (datau) {
                                let user = datau;
                                let cdata = [
                                    {
                                        username: user.name,
                                        email: user.email,
                                        taskid: comment.taskid,
                                        cemail: AppConfig.cemail,
                                        companyName: AppConfig.cname,
                                        mobile: AppConfig.cmobile,
                                        message: "New comment is added by " + comment.userid + " for tast id = " + comment.taskid,
                                        comment: comment.commentBody
                                    }];
                                //code to send notification
                                logger.info("sending emails..............to the users");
                                CommentEmailService.sendEmail(cdata);
                            }
                            callback(err,data);
                        });
                    }
                });
            }
        }
    });
};

exports.findCommentsByCommentId = function (taskid, commentId, callback) {
    CommentsDao.findCommentsByCommentId(taskid, commentId, function (err, ddata) {
        callback(err, ddata);
    });
};

exports.findComments = function (callback) {
    CommentsDao.findComments(function (err, ddata) {
        callback(err, ddata);
    });
};



exports.findCommentsByTaskId = function (taskid, callback) {
    CommentsDao.findCommentsByTaskId(taskid, function (err, comments) {
        callback(err, comments);
    });
};

exports.deleteCommentsByCid = function (taskid, commentid, callback) {
    CommentsDao.deleteCommentsByCommentId(taskid, commentid, function (err) {
        callback(err);
    });
};

exports.updateComments = function (comment, callback) {
    TasksService.findTaskByTaskId(comment.taskid, function (err, data) {
        if (!data || err) {
            err = "Invalid taskid";
            callback(err, data);
        } else {
            let task = data;
            let userid;
            let valid = false;
            if (comment.userid === task.owner) {
                userid = task.assignee;
                valid = true;
            } else if (comment.userid === task.assignee) {
                userid = task.owner;
                valid = true;
            }
            if (!valid) {
                err = "Invalid user or the user is neither the owner nor the assignee of task";
                callback(err, data);
            }
            else {
                    logger.info("Update comment");
                CommentsDao.updateComments(comment, function (err, data) {
                    if (data) {
                        UsersService.findUserByUserId(userid, function (err, datau) {
                            if (datau) {
                                let user = datau;
                                let cdata = [
                                    {
                                        username: user.name,
                                        email: user.email,
                                        taskid: comment.taskid,
                                        cemail: AppConfig.cemail,
                                        companyName: AppConfig.cname,
                                        mobile: AppConfig.cmobile,
                                        message: "Comment is updated by " + comment.userid + " for tast id = " + comment.taskid,
                                        comment: comment.commentBody
                                    }];
                                //code to send notification
                                logger.info("sending emails..............to the users");
                                CommentEmailService.sendEmail(cdata);
                            }
                            callback(err,comment);
                        });
                    } else {
                        callback(err,comment);
                    }
                });
            }
        }
    });
};   