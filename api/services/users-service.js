var UsersDao = require("../persistence/users-dao");
//saving the users into the database
exports.saveUser=function(user,callback) {
    UsersDao.saveUser(user,function(err,data) {
        callback(err,data);
    });
};   

exports.findUserByUserId=function(userId,callback){
    UsersDao.findUserByUserId(userId,function(err,ddata){
        callback(err,ddata);
    });
};

exports.findUsers=function(callback){
    UsersDao.findUsers(function(err,ddata){
        callback(err,ddata);
    });
};

exports.deleteUserByUid=function(userid,callback) {
    UsersDao.deleteUserByUserId(userid,function(err){
        callback(err);
    });      
};

exports.updateUser=function(user,callback){
    UsersDao.updateUser(user,function(err){
        callback(err);
    });      
};   