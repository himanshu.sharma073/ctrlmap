var TasksController = require("../controllers/tasks-controller");
//below is function expression where we are passing e
//instance of express framework as a parameter
module.exports=function(express){  
    express.post("/tasks",TasksController.saveTask);
    express.put("/tasks/:taskid",TasksController.updateTask);
    express.delete("/tasks/:taskid",TasksController.deleteTaskByTid);
    express.get("/tasks/:taskid",TasksController.findTaskByTaskId);
    express.get("/tasks",TasksController.findTasks);
};