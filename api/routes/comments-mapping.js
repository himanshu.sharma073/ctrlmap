
var CommentsController = require("../controllers/comments-controller");
var UsersController = require("../controllers/users-controller");
var TasksController = require("../controllers/tasks-controller");
//below is function expression where we are passing e
//instance of express framework as a parameter
module.exports=function(express){
    
    express.post("/tasks/:taskid/comments",CommentsController.saveComment);
    express.put("/tasks/:taskid/comments",CommentsController.updateComments);
    //find comments by taskid by date in chronological order
    express.get("/tasks/:taskid/comments/",CommentsController.findCommentsByTaskId);
    express.delete("/tasks/:taskid/comments/:cid",CommentsController.deleteCommentsByCid);
    express.get("/tasks/:taskid/comments/:cid",CommentsController.findCommentsByCommentId);
};