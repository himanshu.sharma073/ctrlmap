var UsersController = require("../controllers/users-controller");
//below is function expression where we are passing e
//instance of express framework as a parameter
module.exports=function(express){
    express.post("/users",UsersController.saveUser);
    express.put("/users/:userid",UsersController.updateUser);
    express.delete("/users/:userid",UsersController.deleteUserByUid);
    express.get("/users/:userid",UsersController.findUserByUserId);
    express.get("/users",UsersController.findUsers);

};