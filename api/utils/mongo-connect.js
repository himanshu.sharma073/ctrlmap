/**
 *  This fill will make a
 *  connection to the database
 *  This file is known as module
 */
var mongoose = require('mongoose');

var logger = require('logger').createLogger();

module.exports=function() {
    //below code is used to connect with mongodb using mongoose
    var conn = mongoose.connection;
	conn.on('error', console.error);
	conn.once('open', function() {
		//	Create your schemas and models here.
		logger.info("Opening connection to database!");
		logger.info("MongoDB Database is ready to store data");	
    });
    //hey connect with mongodb
	mongoose.connect('mongodb://localhost:27017/comments_db');
};
