var CommentEntity=require('../odm/comments-entity');
var TaskEntity=require('../odm/tasks-entity');
var UserEntity=require('../odm/users-entity');
var logger = require('logger').createLogger();

module.exports=function(){
   
    var user1 =new UserEntity();
    user1.userid = "himanshu.sharma";
    user1.name = "Himanshu Sharma";
    user1.email = "himanshu.sharma07307@gmail.com"
    user1.save(function(err){
        if(!err)
        logger.info("save in database");        
    });

    var user2 =new UserEntity();
    user2.userid = "javahunk";
    user2.name = "Java Hunk";
    user2.email = "javahunk100@gmail.com"
    user2.save(function(err){
        if(!err)
        logger.info("save in database");        
    });

    var task = new TaskEntity();
    task.taskid = "dummytask";
    task.assignee = user1.userid;
    task.owner = user2.userid;
    task.save(function(err){
        if(!err)
        logger.info("save in database");        
    });

    var comment1 = new CommentEntity();
    comment1.commentId = "dummycomment1";
    comment1.commentBody = "dummy comment 1 body";
    comment1.userid = user1.userid;
    comment1.taskid = task.taskid;
    comment1.save(function(err){
        if(!err)
        logger.info("save in database");        
    });
    var comment2 = new CommentEntity();
    comment2.commentId = "dummycomment2";
    comment2.commentBody = "dummy comment 2 body";
    comment2.userid = user2.userid;
    comment2.taskid = task.taskid;
    comment2.save(function(err){
        if(!err)
        logger.info("save in database");        
    });

         };