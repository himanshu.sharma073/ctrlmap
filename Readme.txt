Steps to run the project::
	1. Change the email and password in file under api/config/email-config.js with valid creds.
	2. use npm install inside the api and web folder.
	3. use "node server.js" to start the rest API.
	4. use ng serve to start the Angular project on port 4200
	5. Access application using below url:
		"http://localhost:4200"